"""Unit tests dedicated to the data preparation routine.
"""

from pathlib import Path

import pandas as pd
import pytest

from albion_similog import prepare_data


@pytest.fixture
def input_filepath():
    yield Path("tests/data/dataseries.csv")


def test_read_and_prepare_data(input_filepath):
    """Test the input data reading process"""
    df = prepare_data.read_and_prepare_data(input_filepath)
    exp_df = pd.DataFrame(
        {
            "API": ["A", "A", "A", "B", "B", "B", "B", "C", "C", "C"],
            "MD": [10.0, 20.0, 30.0, 0.0, 10.0, 20.0, 30.0, 0.0, 10.0, 20.0],
            "ILD": [15.9, 35.0, 43.1, 14.0, 16.2, 18.1, 19.9, 29.9, 25.4, 31.3],
        }
    )
    pd.testing.assert_frame_equal(df, exp_df)


def test_read_and_prepare_data_with_wrong_columns(input_filepath):
    """The provided file expects ['API', 'MD', 'ILD'] columns."""
    with pytest.raises(ValueError):
        prepare_data.read_and_prepare_data(
            input_filepath, columns=["API", "MD", "foo", "bar"]
        )
    with pytest.raises(ValueError):
        prepare_data.read_and_prepare_data(
            input_filepath, columns=["API", "ILD", "foo", "bar"]
        )
    with pytest.raises(ValueError):
        prepare_data.read_and_prepare_data(
            input_filepath, columns=["MD", "ILD", "foo", "bar"]
        )


def test_read_and_prepare_data_extra_columns(input_filepath):
    """Providing too much columns will raise a ValueError when pandas will try to parse the data."""
    with pytest.raises(ValueError):
        prepare_data.read_and_prepare_data(
            input_filepath, columns=["API", "MD", "foo", "ILD", "bar"]
        )


def test_read_and_prepare_data_wrong(input_filepath):
    """A file with more columns that provided will raise an index error when pandas will try to parse
    it.

    """
    with pytest.raises(IndexError):
        prepare_data.read_and_prepare_data(input_filepath, columns=["API", "MD", "ILD"])
