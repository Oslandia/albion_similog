"""Unit tests dedicated to well correlations.
"""

from pathlib import Path

import numpy as np
import pandas as pd
import pytest

from albion_similog import prepare_data, well_correlation


@pytest.fixture
def input_filepath():
    yield Path("tests/data/dataseries.csv")


@pytest.fixture
def input_filepath_real():
    yield Path("tests/data/resistivity_small.csv")


def test_well_correlation_init(input_filepath):
    """Test the creation of a WellCorrelation instance."""
    df = prepare_data.read_and_prepare_data(input_filepath)
    wc = well_correlation.WellCorrelation(df, depth_min=-1e-3, log_normalize=False)
    # Control the dict segmentation for well 'A'
    pd.testing.assert_series_equal(
        pd.Series(wc._segmentation_indices["A"]), pd.Series([0, 1, 2])
    )
    # Control the original data for well 'A'
    exp_dict_data_original = pd.DataFrame(
        {
            "MD": [10.0, 20.0, 30.0],
            "ILD": [15.9, 35.0, 43.1],
        }
    ).set_index("MD")
    pd.testing.assert_frame_equal(wc._dict_data_original["A"], exp_dict_data_original)
    # Control the aggregated data for well 'A'
    exp_dict_data = pd.Series([15.9, 35.0], name="ILD")
    pd.testing.assert_series_equal(wc._dict_data["A"]["ILD"], exp_dict_data)


def test_well_consensus_depth_segment(input_filepath_real):
    """Test if the depth of two consensus with a different min_seg are coherent."""
    df = prepare_data.read_and_prepare_data(input_filepath_real)
    # Compute well correlations with min_seg = 10
    wc10 = well_correlation.WellCorrelation(
        df, depth_min=-1e-3, log_normalize=False, min_seg=10
    )
    wc10.run()
    max_consensus_depth_10 = wc10.consensus["MD"].max()
    # Compute well correlations with min_seg = 5
    wc5 = well_correlation.WellCorrelation(
        df, depth_min=-1e-3, log_normalize=False, min_seg=5
    )
    wc5.run()
    max_consensus_depth_5 = wc5.consensus["MD"].max()
    # Check if the depth of the two consensus are coherent
    assert (
        np.abs(max_consensus_depth_10 - max_consensus_depth_5) / max_consensus_depth_5
        < 0.1
    )


def test_well_consensus_marker_segment(input_filepath_real):
    """Test if the median depths of a propagated marker at the same consensus depth with a different
    min_seg are coherent.

    """
    df = prepare_data.read_and_prepare_data(input_filepath_real)
    # Compute well correlations with min_seg = 10
    wc10 = well_correlation.WellCorrelation(
        df, log_normalize=False, min_seg=10, nb_markers=1
    )
    wc10.run()
    markers_10 = well_correlation.compute_markers(
        wc10.consensus_depth, wc10.depth_match_global, wc10.consensus, "MD"
    )
    depth_med_10 = markers_10["from"].median()
    # Compute well correlations with min_seg = 1
    wc1 = well_correlation.WellCorrelation(
        df, log_normalize=False, min_seg=1, nb_markers=1
    )
    wc1.run()
    markers_1 = well_correlation.compute_markers(
        wc1.consensus_depth, wc1.depth_match_global, wc1.consensus, "MD"
    )
    depth_med_1 = markers_1["from"].median()
    # Check if the depth of the depths are coherent
    assert np.abs(depth_med_10 - depth_med_1) / (depth_med_1 + depth_med_10) < 0.1


def test_compute_markers():
    """Test the marker computation."""
    depth_global = pd.DataFrame(
        {
            "hole_id": np.repeat(list("ABC"), 4),
            "from": [2.0, 12.0, 22.0, 22.0, 9.0, 9.0, 9.0, 9.0, 6.0, 6.0, 16.0, 26.0],
        }
    )
    tops = [1.0, 10.0, 20.0, 30.0]
    consensus = pd.DataFrame(
        {"global_index": [0, 1, 2, 3], "MD": [1.0, 10.0, 20.0, 30.0]}
    )
    markers = well_correlation.compute_markers(tops, depth_global, consensus, "MD")
    expected_markers = pd.DataFrame(
        {
            "hole_id": ["A", "A", "A", "B", "C", "C", "C"],
            "from": [2.0, 12.0, 22.0, 9.0, 6.0, 16.0, 26.0],
            "to": [2.1, 12.1, 22.1, 9.1, 6.1, 16.1, 26.1],
            "code": [0, 1, 2, 0, 1, 2, 3],
        }
    )
    pd.testing.assert_frame_equal(markers.reset_index(drop=True), expected_markers)
