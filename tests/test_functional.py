from pathlib import Path

import pytest

from albion_similog.prepare_data import read_and_prepare_data
from albion_similog.run_example import run


@pytest.fixture
def input_filepath():
    yield Path("tests/data/resistivity.csv")


@pytest.fixture
def output_dir():
    yield Path("tests/data/output")


def test_functional(input_filepath, output_dir):
    """Test that the output remains equivalent to those produced by legacy code."""
    df = read_and_prepare_data(input_filepath)
    run(df, output_dir)
