ROOTDIR=$(CURDIR)
FAMSADIR=$(HOME)/.local/share/FAMSA
FAMSAWORKDIR=famsa-workdir
OUTPUTDIR=output

.PHONY: all
all: install

$(FAMSADIR):
	git clone -b 'v1.6.2' --depth 1 git@github.com:refresh-bio/FAMSA.git $(FAMSADIR)

famsa-patch: $(FAMSADIR)
	cd $(FAMSADIR) ; patch -p0 < $(ROOTDIR)/FAMSA/patch_src-msa.cpp

$(FAMSADIR)/build: $(FAMSADIR)
	cd $(FAMSADIR); make
	rm -f $(HOME)/.local/bin/famsa
	ln -s $(FAMSADIR)/famsa $(HOME)/.local/bin/famsa

.PHONY: famsa-install
famsa-install: $(FAMSADIR) famsa-patch $(FAMSADIR)/build
	cd $(FAMSADIR); make

.PHONY: pre-install
pre-install:
	pip install cython numpy

.PHONY: install
install: famsa-install pre-install
	pip install -r requirements.txt

.PHONY: dev-install
dev-install: famsa-install pre-install
	pip install -r requirements.txt
	pip install -r requirements-dev.txt
	pre-commit install -f

.PHONY: check
check: black flake8 test

.PHONY: flake8
flake8:
	flake8

.PHONY: black
black:
	black --check .

.PHONY: test
test: data
	pytest

.PHONY: clean
clean:
	rm -f $(HOME)/.local/bin/famsa
	rm -rf $(FAMSADIR)
	rm -f $(OUTPUTDIR)/*.csv
	rm -f $(FAMSAWORKDIR)/*.fasta
